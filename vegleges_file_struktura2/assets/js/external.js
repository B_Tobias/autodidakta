﻿
function validatemail(myform) {
    var x = myform.find("input[name=email]").val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Érvénytelen email formátum");
		
    }
	else
		return true;
}									
$(document).ready(function(){
	
	
	var colors=["#6fdc6f","#4d4dff","#cc4400","#0066cc","#ff9999","#ff4d4d"];
	//generate THE color
	var randomColor = colors[Math.floor(Math.random() * 6)];
	//adjust color
	$("#menu").css("background-color",randomColor);
	$(".block>div").css("background-color",randomColor);	
	//főoldal---cikkek---a projektről
	var conts=[
	"<h1>Autodidakta - tanuld, műveld, élvezd, taníts</h1><p>Az autodidakta célja megmutatni hogyan kezdj el bármit, legyen az hobbi, sport, szakma. Az útmutatók segítenek megtenni az első lépéseket, hogy utána az internet adta hatalmas információhalmazt a javadra fordítsd teljsesen ingyen.</p><div class='block mainblock'>Mond el mit hiányolsz! Amint kész, emailben értesítést kapsz róla! <form id='hianyol'><label for='email'>Email címed: </label><input type='text' id='email' type='text' placeholder='pl: valaki@gmail.com' name='email'><br><label for='hianyolt'>Amit hiányolsz:</label><input type='text' id='hianyolt' name='hianyolt' placeholder='Táncstílus, barkácsolás...' type='text'><br><input type='submit'></form></div><div class='block mainblock'>Értesz valamihez, és szeretnéd átadni a tudást? Iratkozz fel, és megkereslek!<br><form id='mihezertesz'><label for='email'>Email címed: </label><input id='email' type='text' placeholder='pl: valaki@gmail.com' name='email'><br><label for='amihez'>Amihez értesz: </label><input id='amihez' type='text' name='skill' placeholder='Hangszer, népművészet...'><br><input type='submit'></form></div>",
	"<br><h1>Tartalomtár</h1><div class='block category'><div>Művészet</div><ul><li><a></a></li><li><a></a></li><li><a></a></li><li><a></a></li></ul></div><div class='block category'><div>Sport</div><ul><li><a></a></li><li><a></a></li><li><a></a></li><li><a></a></li></ul></div><div class='block category'><div>Alkotás</div><ul><li><a></a></li><li><a></a></li><li><a></a></li><li><a></a></li></ul></div><div class='block category'><div>technológia</div><ul><li><a href='arcticles/programozas/arcticle.html'>Programozás</a></li><li><a></a></li><li><a></a></li><li><a></a></li></ul></div>",
	"<h1>A projektről</h1><p>Az autodidakta az interneten fellelhető hatalmas tudás és információhalmazcikkekbe sűrítése. Egy ugródeszka, amely segít elkezdeni. A világon mindent meg lehet már tanulni, méghozzá ingyen. Az egyetlen gondot az ilyen jellegűtanulási folyamatoknál a kezdő lépés nehézsége okozza. </p><p>Az <a href='https://www.epam.com/careers/hungary'>EPAM Systems Kft.</a> álltal hirdetett nyárigyakorlat felvételi versenyére valósítottam meg ezt az ötletet.</p>"
	];
	//show actual contetnt
	$("#menu a").click(function(){

		//change content
		$("#cont").html(conts[$(this).parent().index()]);
		//content color
		$(".block>div").css("background-color",randomColor);
		$(".mainblock").css("background-color",randomColor);
		
		//change menu hover
		$(this).addClass('actual');				
		$("#menu a").not(this).removeClass('actual');	
    });
	
	//content at first time or from link
	if (wichmenu.length==0)
		$("#"+'fooldal').click();
	else
		$("#"+wichmenu).click();


	
	//forms:
		$('#mihezertesz').on('submit', function (e) {
			e.preventDefault();
			if (validatemail($(this))){
				$.ajax({
					type: 'post',
					url: 'ajanlat.php',
					data: $(this).serialize(),
					success: function () {
					alert('Ajánlatod elküldésre került :)');}
          });
		}
        });
		
		$('#hianyol').on('submit', function (e) {
			e.preventDefault();
			if (validatemail($(this))){
				$.ajax({
					type: 'post',
					url: 'hianyolt.php',
					data: $(this).serialize(),
					success: function () {
					alert('Kérésed elküldésre került :)');}
          });
		}
        });

});

